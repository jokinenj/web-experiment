import sqlite3

def create_tables(conn):
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS results (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            participant_id TEXT NOT NULL,
            trial_number INTEGER NOT NULL,
            hidden_number INTEGER NOT NULL,
            action_number INTEGER NOT NULL,
            action_type TEXT NOT NULL,
            score INTEGER NOT NULL
        )
    ''')
    conn.commit()

def main():
    conn = sqlite3.connect('experiment.db')
    create_tables(conn)
    conn.close()

if __name__ == '__main__':
    main()
