from flask import Flask, render_template, request, jsonify
import random
import sqlite3

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/new_trial', methods=['POST'])
def new_trial():
    goal = random.randint(1, 3)
    hidden_number = random.randint(1, 3)
    return jsonify(goal=goal, hidden_number=hidden_number)

@app.route('/open_box', methods=['POST'])
def open_box():
    hidden_number = int(request.form['hidden_number'])
    box_prob = random.random()
    if box_prob <= 0.8:
        revealed_number = hidden_number
    else:
        revealed_number = random.choice([n for n in range(1, 4) if n != hidden_number])
    return jsonify(revealed_number=revealed_number)

@app.route('/save_result', methods=['POST'])
def save_result():
    data = request.get_json()
    save_result_to_db(data['participant_id'], data['trial_number'], data['hidden_number'], data['action_number'], data['action_type'], data['score'])
    return jsonify(success=True)

def save_result_to_db(participant_id, trial_number, hidden_number, action_number, action_type, score):
    conn = sqlite3.connect('experiment.db')
    cursor = conn.cursor()
    cursor.execute('''
        INSERT INTO results (participant_id, trial_number, hidden_number, action_number, action_type, score)
        VALUES (?, ?, ?, ?, ?, ?)
    ''', (participant_id, trial_number, hidden_number, action_number, action_type, score))
    conn.commit()
    conn.close()

if __name__ == '__main__':
    app.run(debug=True)
