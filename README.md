# web-experiment

How to run:

1. Initialize the database (needs to be done only once): `python init_db.py`.

2. Start the server: `python -m flask run`.

3. Navigate to http://127.0.0.1:5000 using a browser.

4. Data is recorded after each button press. Run `python export_data.py` to export from database to .csv.


