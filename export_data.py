import sqlite3
import csv

def read_data_from_db():
    conn = sqlite3.connect('experiment.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM results')
    data = cursor.fetchall()
    conn.close()
    return data

def write_data_to_csv(data, filename):
    with open(filename, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['id', 'participant_id', 'trial_number', 'hidden_number', 'action_number', 'action_type', 'score'])
        for row in data:
            csv_writer.writerow(row)

def main():
    data = read_data_from_db()
    write_data_to_csv(data, 'experiment_data.csv')
    print("Data exported to 'experiment_data.csv'")

if __name__ == '__main__':
    main()
